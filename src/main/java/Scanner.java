import model.Apple;
import model.Orange;
import model.Product;
import model.ProductImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peter on 22/01/2017.
 */
public class Scanner {

    private List<Product> productList = new ArrayList<>();
    private BigDecimal amount = new BigDecimal(0.0);

    public void addToList(String... str) {
        for (int i = 0; i < str.length; i++) {
            addProductToList(str[i].toLowerCase());
        }
    }

    private void addProductToList(String name) {
        if (name.equals(ProductImpl.Name.APPLE.name().toLowerCase())) {
            calculateAndReg(new Apple());
        } else if (name.equals(ProductImpl.Name.ORANGE.name().toLowerCase())) {
            calculateAndReg(new Orange());
        }
    }

    private void calculateAndReg(Product product) {
        productList.add(product);
        int tmp = 0;
        for (Product p : productList) {
            if (p.getName().equals(product.getName())) {
                tmp++;
            }
        }
        if (product.isFree(tmp)) {
            amount = amount.add(product.getPrice());
        }
    }

    public boolean isEmpty() {
        return productList.size() == 0;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
