package model;

import java.math.BigDecimal;

/**
 * Created by peter on 22/01/2017.
 */
public abstract class ProductImpl implements Product {

    public enum Name {
        APPLE,
        ORANGE;
    }

    @Override
    public abstract BigDecimal getPrice();
}
