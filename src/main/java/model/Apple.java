package model;

import java.math.BigDecimal;

/**
 * Created by peter on 22/01/2017.
 */
public class Apple extends ProductImpl {

    private BigDecimal price = new BigDecimal(0.60);
    private int freeEvery = 2;

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    public String getName() {
        return Name.APPLE.name();
    }

    public boolean isFree(int number) {
        return number % freeEvery != 0;
    }
}
