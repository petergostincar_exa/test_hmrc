package model;

import java.math.BigDecimal;

/**
 * Created by peter on 22/01/2017.
 */
public class Orange extends ProductImpl {

    private BigDecimal price = new BigDecimal(0.25);
    private int freeEvery = 3;

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    public String getName() {
        return Name.ORANGE.name();
    }

    public boolean isFree(int number) {
        return number % freeEvery != 0;
    }
}
