package model;

import java.math.BigDecimal;

/**
 * Created by peter on 22/01/2017.
 */
public interface Product {

    BigDecimal getPrice();

    String getName();

    boolean isFree(int number);
}
