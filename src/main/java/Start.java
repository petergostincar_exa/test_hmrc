/**
 * Created by peter on 22/01/2017.
 */
public class Start {

    public static void main(String a[]) {

        Scanner s = new Scanner();
        s.addToList("apple", "apple", "apple", "orange", "orange", "orange");

        s.getProductList().forEach(p -> System.out.println(p.getName() + " -> " + p.getPrice().doubleValue()));
        System.out.println("Total: " + s.getAmount().doubleValue());
    }
}
