import model.Apple;
import model.Orange;
import model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by peter on 22/01/2017.
 */
public class ScannerTest {

    Scanner scanner;

    @Before
    public void before(){
        scanner =  new Scanner();
    }

    @Test
    public void isScannerEmpty() throws Exception {
        assertEquals(true, scanner.isEmpty());
    }

    @Test
    public void isScannerEmpty_after_addToList() throws Exception {
        scanner.addToList(new Apple().getName());

        assertEquals(false, scanner.isEmpty());
    }

    @Test
    public void areAllItemsAdded_addToList() throws Exception {
        scanner.addToList(new Apple().getName());
        scanner.addToList(new Orange().getName());
        assertEquals(true, scanner.getProductList().size() > 1);
    }

    @Test
    public void checkAmount_addToList() throws Exception {
        Product apple = new Apple();
        Product orange = new Orange();

        scanner.addToList(apple.getName());
        scanner.addToList(apple.getName());
        scanner.addToList(orange.getName());
        scanner.addToList(orange.getName());
        scanner.addToList(orange.getName());

        BigDecimal amount = apple.getPrice();
        amount = amount.add(orange.getPrice());
        amount = amount.add(orange.getPrice());
        assertEquals(amount.toString(), scanner.getAmount().toString());
    }

}